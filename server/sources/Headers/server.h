//
// Created by ata on 3/18/17.
//

#ifndef AP_PROJEC1_SERVER_H
#define AP_PROJEC1_SERVER_H
#include <QUdpSocket>
#include <QTcpSocket>
#include <QTcpServer>
#include <QObject>
#include <QApplication>

class Server:public QObject
{
Q_OBJECT
public:
    Server(QHostAddress * =new QHostAddress("127.0.0.1") , quint16 =8000);
public slots:
    void newclient();
    void exp();
private:
    QList<QTcpSocket*> clientlist;
    QTcpServer server;

};

#endif //AP_PROJEC1_SERVER_H
