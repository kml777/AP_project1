//
// Created by ata on 3/18/17.
//

#include "../Headers/server.h"
#include <QObject>

Server::Server(QHostAddress *addr,quint16 prt)
{
    server.listen(*addr,prt);
//   QObject::connect(&server,SIGNAL(newConnection()),this,SLOT(newclient()));
}

void Server::newclient()
{
    QTcpSocket *nc=server.nextPendingConnection();
    clientlist.push_back(nc);
    connect(nc,SIGNAL(readyread),this,SLOT(exp));
}

void Server::exp()
{
    for(int i=0;i<clientlist.size();i++)
        if(clientlist[i] -> bytesAvailable())
        {
            QByteArray s=clientlist[i]->readAll();
            for(int j=0;j<clientlist.size();j++)
                clientlist[i] -> write(s);
        }
}