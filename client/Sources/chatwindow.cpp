//
// Created by ata on 3/18/17.
//
#include<QtWidgets/QPlainTextEdit>
#include "../Headers/chatwindow.h"


chatwindow::chatwindow(int width, int height, QString name, QWidget *parent)
        : QWidget(parent), name(name)
{
    QRect rec = QApplication::desktop() -> screenGeometry();
    setGeometry(rec.width() / 2 - width / 2, rec.height() / 2 - height / 2, width, height);
   //set secreen

    messages.setReadOnly(1);
    messages.setFixedHeight(2*height/3);
   //set messagebox location

    input.setFixedHeight(height/3);
   //set input location

    connect(&input,&QTextEdit::textChanged,this,&chatwindow::send_by_enter);
    connect(&Send,&QPushButton::clicked,this,&chatwindow::send_message);
   //set signals and slots for sending first for send by enter and second by send bye Send button

    sound.setMedia((QUrl::fromLocalFile(QFileInfo("../backcourse_marker.wav").absoluteFilePath())));
    //set notification sound :D

    QVBoxLayout layout;
    layout.addWidget(&messages);
    layout.addWidget(&input);
    layout.addWidget(&Send);
    setLayout(&layout);
    //add all widgets to a layout

    show();
}

inline bool check(QString s,QString t)
{
    if(s.length()<=t.length())
        return 1;
    for(int i=0;i<t.length();i++)
        if(s[i]!=t[i])
            return 1;
    if(s[t.length()]==':')
        return 0;
    return 1;
}//check if the sender is not name returns true

void chatwindow::recive_message(QString s)
{
    messages.append(s);
    if(check(s,name))
        sound.play();
}
//a slot which activated when new message received maybe own message but in this case notification sound doesn't play

void chatwindow::send_message()
{
    QString s=input.toPlainText();
    input.clear();
    if(s.length())
            emit message_sent(name+"::"+s);//signal to interface
}
//a slot that emits when using send button

void chatwindow::send_by_enter()
{
    QString s=input.toPlainText();
    input.clear();
    if(s.length()&&s[s.length()-1] == '\n')
            emit message_sent(name+"::"+s);//signal to interface
}
//a slot that emits when using send button
//note that these 2 signals work on interface class