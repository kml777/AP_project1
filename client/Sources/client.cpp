//
// Created by ata on 3/19/17.
//

#include "../Headers/client.h"

client::client()
{
    UI=new interface(this);
    connect(UI->lm,&loginmenu::Login_signal,this,&client::_connect);
}


void client::_connect(QString s, QHostAddress server, quint16 port)
{
    user=s;
    __connection=new connection(server,port);
    connect(__connection,&connection::new_message,UI->cw,&chatwindow::recive_message);
    connect(UI->cw,&chatwindow::message_sent,__connection,&connection::send_message);
}