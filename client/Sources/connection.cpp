//
// Created by ata on 3/19/17.
//

#include "../Headers/connection.h"

connection::connection(QHostAddress address, quint16 port)
{
    sc.connectToHost(address,port);
    connect(&sc,&QTcpSocket::readyRead,this,&connection::recive_message);
}

void connection::recive_message()
{
    QString s=sc.readAll();
    emit new_message(s);
}

void connection::send_message(QString s)
{
    sc.write(s.toUtf8());
}
