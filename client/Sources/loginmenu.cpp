//
// Created by ata on 3/18/17
//

#include "../Headers/loginmenu.h"

loginmenu::loginmenu(int width, int height, QWidget *parent)
: QWidget(parent)
{
    QRect rec = QApplication::desktop() -> screenGeometry();
    setGeometry(rec.width() / 2 - width / 2, rec.height() / 2 - height / 2, width, height);

    connect(&login,&QPushButton::clicked,this,&loginmenu::Login);
    connect(&exit,&QPushButton::clicked,this,&loginmenu::close);
    QVBoxLayout box1;
    box1.addWidget(&username);
    box1.addWidget(&address);
    box1.addWidget(&port);
    QHBoxLayout box2;
    box2.addWidget(&login);
    box2.addWidget(&exit);

    QVBoxLayout box3;
    box3.addLayout(&box1);
    box3.addLayout(&box2);

    setLayout(&box3);
    show();
}

void loginmenu::Login()
{
    emit Login_signal(username.text(),QHostAddress(address.text()),port.text().toUShort());
    hide();
}