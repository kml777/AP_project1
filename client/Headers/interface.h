//

// Created by ata on 3/19/17.
//

#ifndef AP_PROJEC1_INTERFACE_H
#define AP_PROJEC1_INTERFACE_H

#include "chatwindow.h"
#include "loginmenu.h"
#include <QObject>

class loginmenu;
class chatwnidow;
class QHostaddress;

class interface:public QObject
{
Q_OBJECT
public:
    interface(QObject * = 0);
    chatwindow *cw;
    loginmenu *lm;
public slots:
    void logedin(QString,QHostAddress =QHostAddress("127.0.0.1"),quint16 =8000);

};

#endif //AP_PROJEC1_INTERFACE_H
