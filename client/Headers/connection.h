//
// Created by ata on 3/19/17.
//

#ifndef AP_PROJEC1_CONNECTION_H
#define AP_PROJEC1_CONNECTION_H

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>

class connection:public QObject
{
Q_OBJECT
public:
    connection(QHostAddress ,quint16);

private:
    QTcpSocket sc;

public slots:
    void recive_message();
    void send_message(QString);
    signals:
    void new_message(QString);
};

#endif //AP_PROJEC1_CONNECTION_H
