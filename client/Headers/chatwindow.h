//
// Created by ata on 3/18/17.
//

#ifndef AP_PROJEC1_CHATWINDOW_H
#define AP_PROJEC1_CHATWINDOW_H

#include <QPushButton>
#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QTextEdit>
#include <QHBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QMediaPlayer>
#include <QShortcut>
#include <QFileInfo>

class chatwindow:public QWidget
{
Q_OBJECT
public:
    chatwindow(int, int, QString, QWidget * = 0);

public slots:
    void recive_message(QString );//works in interface
    void send_message();
    void send_by_enter();
private:
    QTextEdit input,messages;
    QPushButton Send;
    QString name;
    QMediaPlayer sound;

signals:
    void message_sent(QString );
};


#endif //AP_PROJEC1_CHATWINDOW_H
