//
// Created by ata on 3/18/17.
//

#ifndef AP_PROJEC1_LOGINMENU_H
#define AP_PROJEC1_LOGINMENU_H

#include <QWidget>
#include <QPushButton>
#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QLineEdit>
#include <QSpinBox>
#include <QLabel>
#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QHostAddress>


class loginmenu: public QWidget
{
Q_OBJECT
public:
    loginmenu(int, int, QWidget * = 0);


private:
    QLineEdit username,address,port;
    QPushButton login,exit;
signals:
    void Login_signal(QString,QHostAddress =QHostAddress("127.0.0.1"),quint16 =8000);

public slots:
    void Login();
};



#endif //AP_PROJEC1_LOGINMENU_H
