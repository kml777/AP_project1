//
// Created by ata on 3/19/17.
//

#ifndef AP_PROJEC1_CLIENT_H
#define AP_PROJEC1_CLIENT_H

#include "connection.h"
#include "interface.h"
#include <QObject>
#include <QHostAddress>

class client:public QObject
{
Q_OBJECT
public:
    client();

private:
    QString user;
    interface *UI;
    connection *__connection;
public slots:
    void _connect(QString ,QHostAddress =QHostAddress("127.0.0.1"),quint16 =8000);

};

#endif //AP_PROJEC1_CLIENT_H
